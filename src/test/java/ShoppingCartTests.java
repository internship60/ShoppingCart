import com.oorjaa.Product;
import com.oorjaa.ShoppingCart;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class ShoppingCartTests {

    private ShoppingCart shoppingCart = null;
    private Product doveSoap;
    private Product axeDeo;

    @BeforeEach
    public void setup() {
        
        shoppingCart = new ShoppingCart();
    }

    @Test
    public void testAddFiveProductsToCart() {
//       GIVEN:
        doveSoap = new Product("Dove Soap", 39.99, 5);

//       WHEN:
        shoppingCart.addToCart(doveSoap);

//       THEN:

        /* The shopping cart should contain 5 Dove Soaps **/
        Assertions.assertEquals(5, shoppingCart.getProductQuantity(doveSoap));

        /* The Price of each unit should be 39.99 */
        Assertions.assertEquals(39.99, shoppingCart.getUnitPrice(doveSoap));

        /* The shopping cart’s total price should equal 199.95 */
        Assertions.assertEquals(199.95, shoppingCart.getTotalPriceWithOutTax());
    }

    @Test
    public void testAddAdditionalThreeProductToCart() {
//       WHEN:
        doveSoap = new Product("Dove Soap", 39.99, 5);
        shoppingCart.addToCart(doveSoap);
        doveSoap = new Product("Dove Soap", 39.99, 3);
        shoppingCart.addToCart(doveSoap);

//       THEN:
        /* The shopping cart should contain 8 Dove Soaps **/
        Assertions.assertEquals(8, shoppingCart.getProductQuantity(doveSoap));

        /* The Price of each unit should be 39.99 **/
        Assertions.assertEquals(39.99, shoppingCart.getUnitPrice(doveSoap));

        /* The shopping cart’s total price should equal 319.92 **/
        Assertions.assertEquals(319.92, shoppingCart.getTotalPriceWithOutTax());
    }

    @Test
    public void testAddMultipleProductsToCartWithTaxRate() {
//       GIVEN:
        double TAX_RATE = 12.5;

//       WHEN:
        doveSoap = new Product("Dove Soap", 39.99, 2);
        shoppingCart.addToCart(doveSoap);
        axeDeo = new Product("Axe Deo", 99.99, 2);
        shoppingCart.addToCart(axeDeo);


//       THEN:

        /* The shopping cart should contain 2 Dove Soaps each with a unit price of 39.99 **/
        Assertions.assertEquals(2, shoppingCart.getProductQuantity(axeDeo));
        Assertions.assertEquals(99.99, shoppingCart.getUnitPrice(axeDeo));

        /* The shopping cart should contain 2 Axe Deos each with a unit price of 99.99 **/
        Assertions.assertEquals(2, shoppingCart.getProductQuantity(doveSoap));
        Assertions.assertEquals(39.99, shoppingCart.getUnitPrice(doveSoap));

        /* the shopping cart’s total price should equal 314.96 **/
        Assertions.assertEquals(314.96, shoppingCart.getTotalPriceWithTax());
    }
}