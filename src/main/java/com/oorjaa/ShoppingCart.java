package com.oorjaa;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

public class ShoppingCart {

    private static DecimalFormat decimalFormat = new DecimalFormat("0.00");
    private List<Product> cart = new ArrayList<>();


    public void addToCart(Product product) {
        boolean exists = false;
        if (!cart.isEmpty()) for (Product prod : cart) {
            if (prod.getProductName().equals(product.getProductName())) {
                prod.setQuantity(prod.getQuantity() + product.getQuantity());
                exists = true;
            }
        }
        if (!exists) {
            cart.add(product);
        }
    }

    public int getProductQuantity(Product product) {
        int quantity = 0;
        for (Product prod : cart) {
            if (prod.getProductName().equals(product.getProductName())) {
                quantity = prod.getQuantity();
            }
        }
        return quantity;
    }

    public double getUnitPrice(Product product) {
        double unitPrice = 0;
        for (Product prod : cart) {
            if (prod.getProductName().equals(product.getProductName())) {
                unitPrice = prod.getPrice();
            }
        }
        return unitPrice;
    }

    public double getTotalPriceWithTax() {
        double SALES_TAX = 12.5;
        double taxAmount = Math.ceil(getTotalPriceWithOutTax() / 100 * SALES_TAX);
        double totalPriceWithTax = getTotalPriceWithOutTax() + taxAmount;
        return Double.parseDouble(decimalFormat.format(totalPriceWithTax));
    }

    public double getTotalPriceWithOutTax() {
        double totalPrice = 0;
        for (Product product: cart) {
            totalPrice = totalPrice + product.price * getProductQuantity(product);
        }
        return Double.parseDouble(decimalFormat.format(totalPrice));
    }
}
